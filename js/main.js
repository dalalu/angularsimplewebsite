var app = angular.module("simplewebsite", ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider
    .when('/404', {
      templateUrl: 'templates/404.html',
      controller: '404Ctrl'
    })
    .when('/main', {
      templateUrl: 'templates/main.html',
      controller: 'MainCtrl'
    })
    .when('/about', {
      templateUrl: 'templates/about.html',
      controller: 'AboutCtrl'
    })
    .when('/services', {
      templateUrl: 'templates/services.html',
      controller: 'ServicesCtrl'
    })
    .when('/contact', {
      templateUrl: 'templates/contact.html',
      controller: 'ContactCtrl'
    })
    .otherwise({redirectTo:'/main'})
}])

.controller('MainCtrl', ['$scope', function($scope) {
  $scope.person = 'Julien Lavie';
  console.log($scope);
}])

.controller('AboutCtrl', ['$scope', function($scope) {
  
}])

.controller('ServicesCtrl', ['$scope', '$http', function($scope, $http) {
  $http.get('json/services.json').then(function(res) {
	  console.log('test');
    $scope.services = res.data;
  });
}])

.controller('ContactCtrl', ['$scope', function($scope) {
  
}])
